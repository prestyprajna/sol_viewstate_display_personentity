﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_ViewState_Task.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="firstName" runat="server" Text="FirstName"></asp:TextBox>
        <asp:TextBox ID="lastName" runat="server" Text="LastName"></asp:TextBox>
        <asp:TextBox ID="age" runat="server" Text="Age"></asp:TextBox>

        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
    
    </div>
    </form>
</body>
</html>
