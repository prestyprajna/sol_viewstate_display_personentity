﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_ViewState_Task
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            PersonEntity personEntityObj = new PersonEntity()
            {
                FirstName = "presty",
                LastName = "prajna",
                age = 23
            };

            if (this.ViewState["Entity"] != null)
            {

                this.ViewState["Entity"] = personEntityObj;

                personEntityObj = ViewState["Entity"] as PersonEntity;
            }

            //bind in textbox

            firstName.Text = personEntityObj.FirstName;
            lastName.Text = personEntityObj.LastName;
            age.Text = personEntityObj.age.ToString();          

        }
    }

    public class PersonEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int age { get; set; }
    }
}